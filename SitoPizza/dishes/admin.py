from django.contrib import admin
from .models import Dish, Topping, OrderedDish, ToppingOrderedDishRelationship
# Register your models here.

class DishAdmin(admin.ModelAdmin):
	pass

class ToppingAdmin(admin.ModelAdmin):
	pass

class OrderedDishAdmin(admin.ModelAdmin):
    pass

class ToppingOrderedDishRelationshipAdmin(admin.ModelAdmin):
	pass

admin.site.register(OrderedDish, OrderedDishAdmin)
admin.site.register(Dish, DishAdmin)
admin.site.register(Topping, ToppingAdmin)
admin.site.register(ToppingOrderedDishRelationship, ToppingOrderedDishRelationshipAdmin)