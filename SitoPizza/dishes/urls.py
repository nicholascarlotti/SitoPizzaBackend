from django.conf.urls import url
from .views import DishDetails, RestaurantDishes, restaurant_cathegory_toppings, restaurant_menu_cathegory_dishes,\
restaurant_toppings, ToppingDetails


urlpatterns = [
	url(r"^menu/?$", RestaurantDishes.as_view()),
    url(r"^menu/(?P<cathegory>[a-z]+)/?$", restaurant_menu_cathegory_dishes),
    url(r"^menu/(?P<pk>[0-9]+)/?$", DishDetails.as_view()),
    url(r"^toppings/(?P<cathegory>[a-z]+)/?$", restaurant_cathegory_toppings),
    url(r"^toppings/(?P<topping_pk>[0-9]+)/?$", ToppingDetails.as_view()),
    url(r"^toppings/?$", restaurant_toppings),
]