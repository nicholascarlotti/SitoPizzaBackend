from django.test import TestCase
from rest_framework.test import APITestCase
from django.contrib.auth.models import User as DJuser
from users.models import Employee
from restaurants.models import Restaurant
from users.models import User
import json

class DishesCreationTest(APITestCase):

    fixtures = ['test_fixture',]

    def test_create_dish_allowed(self):
        """
        Create a new dish for a restaurant for which
        we have permission.
        """
        url = "/restaurants/1/menu/"
        django_user = DJuser.objects.get(username = "aziz")
        user = Employee.objects.get(django_user = django_user)
        self.client.force_authenticate(django_user)
        data = {
            "name" : "Pizza 4 Stagioni",
            "price" : "5.50",
            "menu_cathegory" : "pizza",
            "toppings_cathegory" : "pizza",
            "toppings" : []
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 201)

    def test_create_dish_not_allowed(self):
        """
        Create a new dish for a restaurant for which
        we don't have permission.
        """
        url = "/restaurants/1/menu/"
        django_user = DJuser.objects.get(username = "ling")
        user = Employee.objects.get(django_user = django_user)
        self.client.force_authenticate(django_user)
        data = {
            "name" : "Pizza 4 Stagioni",
            "price" : "5.50",
            "menu_cathegory" : "pizza",
            "toppings_cathegory" : "pizza",
            "toppings" : []
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 403)

    def test_create_dish_simple_user(self):
        url = "/restaurants/1/menu/"
        django_user = DJuser.objects.get(username = "lsdpirate")
        user = User.objects.get(django_user = django_user)
        self.client.force_authenticate(django_user)
        data = {
            "name" : "Pizza 4 Stagioni",
            "price" : "5.50",
            "menu_cathegory" : "pizza",
            "toppings_cathegory" : "pizza",
            "toppings" : []
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 403)

    def test_create_dish_incomplete_data(self):
        url = "/restaurants/1/menu/"
        django_user = DJuser.objects.get(username = "aziz")
        user = User.objects.get(django_user = django_user)
        self.client.force_authenticate(django_user)
        data = {
            "name" : "Pizza 4 Stagioni",
            "price" : "5.50",
            "menu_cathegory" : "pizza",
            "toppings" : []
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 400)

    def test_create_dish_with_toppings(self):
        url = "/restaurants/1/menu/"
        django_user = DJuser.objects.get(username = "aziz")
        user = User.objects.get(django_user = django_user)
        self.client.force_authenticate(django_user)
        data = {
            "name" : "Pizza 4 Stagioni",
            "price" : "5.50",
            "menu_cathegory" : "pizza",
            "toppings_cathegory" : "pizza",
            "toppings" : [
                {
                    "id" : 1
                }
            ]
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 201)
        
    def test_create_dish_wrong_topping(self):
        url = "/restaurants/1/menu/"
        django_user = DJuser.objects.get(username = "aziz")
        user = User.objects.get(django_user = django_user)
        self.client.force_authenticate(django_user)
        data = {
            "name" : "Pizza 4 Stagioni",
            "price" : "5.50",
            "menu_cathegory" : "pizza",
            "toppings_cathegory" : "pizza",
            "toppings" : [
                {
                    "id" : 3
                }
            ]
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 400)

class ToppingsTest(APITestCase):
    fixtures = ['test_fixture',]

    def test_create_topping(self):
        url = "/restaurants/1/toppings/"
        data = {
            "name" : "Peperoni",
            "price" : "1.00",
            "cathegory" : "pizza"
        }
        django_user = DJuser.objects.get(username = 'aziz')
        self.client.force_authenticate(django_user)
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        try:
            self.assertEquals(response.status_code, 201)
        except AssertionError as e:
            print(response.data)
            raise e

    def test_create_topping_permission_denied(self):
        url = "/restaurants/1/toppings/"
        data = {
            "name" : "Peperoni",
            "price" : "1.00",
            "cathegory" : "pizza"
        }
        django_user = DJuser.objects.get(username = 'ling')
        self.client.force_authenticate(django_user)
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        try:
            self.assertEquals(response.status_code, 403)
        except AssertionError as e:
            print(response.data)
            raise e

    def test_create_topping_invalid_data(self):
        url = "/restaurants/1/toppings/"
        data = {
            "name" : "Peperoni",
            "cathegory" : "pizza"
        }
        django_user = DJuser.objects.get(username = 'aziz')
        self.client.force_authenticate(django_user)
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        try:
            self.assertEquals(response.status_code, 400)
        except AssertionError as e:
            print(response.data)
            raise e


    def test_create_topping_anonymous_user(self):
        """
        It should not be possible for an anonymous user to 
        create a topping.
        IMPORTANT: The server should be returning a 401 response but some middleware
        is making it return 403 instead.
        """
        url = "/restaurants/1/toppings/"
        data = {
            "name" : "Peperoni",
            "price" : "1.00",
            "cathegory" : "pizza"
        }
        response = self.client.post(url, json.dumps(data), content_type = "application/json")
        try:
            self.assertEquals(response.status_code, 403)
        except AssertionError as e:
            print(response.data)
            raise e

class ToppingsUpdateTest(APITestCase):
    
    fixtures = ["test_fixture",]
    def test_update_topping(self):
        url = "/restaurants/1/toppings/1/"
        data = {
            "name" : "Funghi"
        }
        user = DJuser.objects.get(username = 'aziz')
        self.client.force_authenticate(user)
        response = self.client.put(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 200)
        parsed_response = response.json()
        self.assertEquals(parsed_response['name'], "Funghi")
        self.assertTrue('id' in parsed_response)

    def test_update_topping_not_allowed(self):
        url = "/restaurants/1/toppings/1/"
        data = {
            "name" : "Funghi"
        }
        user = DJuser.objects.get(username = 'ling')
        self.client.force_authenticate(user)
        response = self.client.put(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 403)
        

    def test_update_topping_anonymous(self):
        url = "/restaurants/1/toppings/1/"
        data = {
            "name" : "Funghi"
        }
        response = self.client.put(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 403)
    
    def test_update_topping_non_employee(self):
        url = "/restaurants/1/toppings/1/"
        data = {
            "name" : "Funghi"
        }
        user = DJuser.objects.get(username = 'lsdpirate')
        self.client.force_authenticate(user)
        response = self.client.put(url, json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 404)

    def test_update_topping_invalid_data(self):
        url = "/restaurants/1/toppings/1/"
        data = {
            "cathegory" : "random"
        }
        user = DJuser.objects.get(username = 'aziz')
        self.client.force_authenticate(user)
        response = self.client.put(url, json.dumps(data), content_type = "application/json")
        self.assertNotEquals(response.json()['cathegory'], "random")

    def test_delete_topping(self):
        url = "/restaurants/1/toppings/1/"
        user = DJuser.objects.get(username = 'aziz')
        self.client.force_authenticate(user)
        response = self.client.delete(url)
        self.assertEquals(response.status_code, 204)
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)
