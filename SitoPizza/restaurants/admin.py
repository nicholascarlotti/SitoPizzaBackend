from django.contrib import admin
from .models import Restaurant, OpeningHours
# Register your models here.


class RestaurantAdmin(admin.ModelAdmin):
	pass

class OpeningHoursAdmin(admin.ModelAdmin):
	pass

admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(OpeningHours, OpeningHoursAdmin)
