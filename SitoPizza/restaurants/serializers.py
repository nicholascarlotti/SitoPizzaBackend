from rest_framework import serializers
from .models import Restaurant
from SitoPizza.serializers import MoneySerializer

class RestaurantSerializer(serializers.ModelSerializer):
    order_minimum = MoneySerializer()
    shipping_cost = MoneySerializer()
    id = serializers.IntegerField(source = "pk", read_only = True)

    class Meta:
        model = Restaurant
        fields = ("id", "name", "address", "street_number", "city", "region", "phone_number",
                "order_minimum", "shipping_cost")
