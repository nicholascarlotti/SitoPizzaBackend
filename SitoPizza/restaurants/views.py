from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, PermissionDenied
from .models import Restaurant
from django.core.exceptions import ObjectDoesNotExist
from .serializers import RestaurantSerializer
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from users.models import Employee
from django.http import HttpResponse

@api_view(['GET', 'POST'])
@csrf_exempt
def restaurants_view(request):
    if request.method == "GET":
        restaurants = Restaurant.objects.all()
        serializer = RestaurantSerializer(restaurants, many = True)
        return Response(serializer.data)
    elif request.method == "POST":
        if not request.user.is_superuser:
            raise PermissionDenied(detail = "You cannot create a restaurant")
        # it is not necessary to use a json parser as it looks like
        # a parsing is already being done by some middleware before the view function
        # gets executed.
        # It turns out that JSONParser is a default parsing class in DRF's config.
        # data = JSONParser().parse(request)
        print(type(request.data))
        print(request.data)
        serializer = RestaurantSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = 201)
        else:
            return Response(serializer.errors, status = 400)


@api_view(["GET", "PUT", "DELETE"])
def restaurant_details(request, pk):
    if request.method == "GET":
        restaurant = get_object_or_404(Restaurant, pk=pk)
        serializer = RestaurantSerializer(instance = restaurant)
        return Response(serializer.data)

    elif request.method == "PUT":
        if not request.user.is_authenticated():
            raise PermissionDenied(detail = "You cannot modify this restaurant")
        restaurant = get_object_or_404(Restaurant, pk=pk)
        try:
            employee = Employee.objects.get(django_user = request.user)
        except ObjectDoesNotExist:
            employee = None
        if not employee or not (employee.restaurant == restaurant):
            raise PermissionDenied(detail = "You cannot modify this restaurant")
        serializer = RestaurantSerializer(instance = restaurant, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = 200)
        else:
            return Response(serializer.erros, status = 400)

    elif request.method == "DELETE":
        if not request.user.is_authenticated():
            raise PermissionDenied(detail = "You cannot modify this restaurant")
        restaurant = get_object_or_404(Restaurant, pk=pk)
        user = request.user
        if not user or not user.is_superuser:
            raise PermissionDenied(detail = "You cannot modify this restaurant")
        restaurant.delete()
        return HttpResponse(status = 200)        
