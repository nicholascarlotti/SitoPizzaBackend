from django.conf.urls import url, include
from .views import restaurants_view, restaurant_details

urlpatterns = [
    url(r"^(?P<pk>[0-9]+)/?$", restaurant_details),
    url(r"^(?P<restaurant_pk>[0-9]+)/", include("dishes.urls")),
    url(r"^$", restaurants_view),
]