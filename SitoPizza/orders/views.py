from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from .models import Order
from .serializers import OrderSerializer, OrderDetailedSerializer,\
	EmployeeModifiableOrderSerializer, OrderIssueSerializer
from users.models import User
from django.contrib.auth.models import User as DJuser
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from .permissions import HasOrderStatusPermission, HasOrderViewPermission
from django.shortcuts import get_object_or_404
from rest_framework.serializers import ValidationError
import datetime

class OrderDetails(APIView):
    
    permission_classes = ((IsAuthenticated, HasOrderStatusPermission,))
    
    def get_object(self, pk):
        order = get_object_or_404(Order, pk = pk)
        self.check_object_permissions(self.request, order)
        return order

    def get(self, request, pk, format = None):
        """
        Get the detailed data of an order.
        """
        order = self.get_object(pk)
        serializer = OrderDetailedSerializer(instance = order)
        return Response(serializer.data, status = 200)

    def put(self, request, pk, format = None):
        """
        Update an order's status field. Can only be performed
        by employees who work in the order's restaurant.
        """
        order = self.get_object(pk)
        serializer = EmployeeModifiableOrderSerializer(instance = order, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = 200)
        else:
            return Response(serializer.errors, status = 400)


class UserOrders(APIView):
    
    permission_classes = ((IsAuthenticated, HasOrderViewPermission))

    def get(self, request, format = None):
        """
        Get all of a user's orders in a non detailed form.
        """
        user = get_object_or_404(User, django_user = request.user)
        orders = Order.objects.filter(user = user)
        serializer = OrderSerializer(orders, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        """
        Create a new order from the request data.
        """
        user = get_object_or_404(User, django_user = request.user)
        data = request.data
        data["user"] = user
        serializer = OrderIssueSerializer(data = request.data)
        if serializer.is_valid(raise_exception = True):
            serializer.save()
            return Response(serializer.data, status = 201)
        else:
            return Response(serializer.errors, status = 400)
