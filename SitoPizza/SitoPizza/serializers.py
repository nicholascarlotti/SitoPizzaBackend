from moneyed import Money
from rest_framework import serializers

class MoneySerializer(serializers.Field):

    def to_representation(self, obj):
        return {
            'amount' : "{}".format(obj.amount),
            'currency' : "{}".format(obj.currency)
        }

    def to_internal_value(self, data):
        """
        Since this method is used for deserialization only and that
        our API won't ask our users to specify currencies we assume
        the "EUR" currency for all user inserted data.
        """
        return Money(data, "EUR")

