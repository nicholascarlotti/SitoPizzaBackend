from rest_framework import serializers
from .models import User
from django.conf import settings
from django.contrib.auth.models import User as DUser

class DjangoUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = DUser
		fields = ('username', 'email')

class UserSerializer(serializers.ModelSerializer):
	django_user = DjangoUserSerializer(many = False, read_only = True)
	class Meta:
		model = User
		fields = ('django_user', 'address', 'street_number', 'city', 'phone_number')
